﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebScraper2
{
    public class HtmlExtractor
    {

        public List<Product> ExtractProductInfo(HtmlDocument htmlFile, List<string> productNames
            , List<string> productPrices, List<string> productRatings, List<Product> productsList)
        {

            try
            {

                foreach (var title in htmlFile.DocumentNode
                    .SelectNodes("//a/img[@alt]"))
                {

                    productNames.Add(HttpUtility.HtmlDecode(title.Attributes["alt"].Value));
                }

                foreach (var price in htmlFile.DocumentNode
                    .SelectNodes("//p/span/span")
                        .Where(n => n.InnerText.Contains("$")))
                {
                    productPrices.Add(price.InnerText.Replace('$', ' ').Trim());
                }

                foreach (var rating in htmlFile.DocumentNode.SelectNodes("//div[@rating]"))
                {
                    var testValue = 0.0;

                    if (double.TryParse(rating.Attributes["rating"].Value.Replace('.', ',').Trim(), out testValue) && testValue <= 5.0)
                    {
                        productRatings.Add(rating.Attributes["rating"].Value);
                    }

                }

                for (int i = 0; i < productRatings.Count(); i++)
                {
                    Product product = new Product(productNames.ElementAt(i), productPrices.ElementAt(i), productRatings.ElementAt(i));
                    productsList.Add(product);

                }

                return productsList;

            }

            catch (NullReferenceException e)
            {
                Console.WriteLine("Unrecognized HTML information.");
                Console.WriteLine("Exception: " + e.Message);
                return null;
            }

            catch (Exception e)
            {
                Console.WriteLine("Something went wrong.");
                Console.WriteLine("Exception: " + e.Message);
                return null;
            }

        }
    }
}
