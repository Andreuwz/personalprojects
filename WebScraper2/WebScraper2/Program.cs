﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace WebScraper2
{
    public class Program
    {
    
        static void Main(string[] args)
        {

            List<string> productNames = new List<string>();
            List<string> productPrices = new List<string>();
            List<string> productRatings = new List<string>();

            List<Product> products = new List<Product>();

            HtmlLoader loader = new HtmlLoader();
            var htmlFile = loader.LoadHtmlFile();

            HtmlExtractor extractor = new HtmlExtractor();
            var resultList = extractor.ExtractProductInfo(htmlFile, productNames, productPrices, productRatings, products);

            string jsonResult = JsonConvert.SerializeObject(resultList, Formatting.Indented);

            Console.WriteLine(jsonResult);
            Console.ReadKey();

        }
    }
}
