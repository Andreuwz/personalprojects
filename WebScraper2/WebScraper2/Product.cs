﻿namespace WebScraper2
{
    public class Product
    {
        public string ProductName { get; }

        public string Price { get; }

        public string Rating { get; }

        public Product(string productName, string price, string rating)
        {
            ProductName = productName;
            Price = price;
            Rating = rating;
        }

        public override string ToString()
        {
            return ProductName + "\n" + Price + "\n" + Rating;
        }

    }
}
