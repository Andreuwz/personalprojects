﻿using HtmlAgilityPack;
using System.IO;

namespace WebScraper2
{
    public class HtmlLoader
    {

        public string HtmlData { get; private set; }

        public HtmlDocument LoadHtmlFile()
        {
            HtmlData = File.ReadAllText(@"HtmlExcerpt.html");

            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(HtmlData);

            return htmlDoc;
        }

    }
}
